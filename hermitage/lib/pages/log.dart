// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hermitage/main.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool hidePassword = true;
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();

  String username = "";
  String password = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF10323A),
      appBar: AppBar(
        backgroundColor: Color(0xFF020215),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: TextButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MyApp()),
            );
          },
          child: Row(
            children:[
              Container(
                alignment: Alignment.center,// use aligment
                child: Image.asset("assets/images/icon.png",
                  height: 30,
                  width: 30,
                fit: BoxFit.cover),
              ),
              const SizedBox(
                width: 10.0,
              ),
              const Text(
                'HERMITAGE',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20
                ),
              ),
            ],
          )
        ),
      ),

      body: Padding(
        padding: EdgeInsets.only(top: 120, left: 24, right: 24),
        child: Center(
          child: Column(
            children: [
              SizedBox(height: 75),
              Text(
                'Selamat Datang, Pengurus Rumah Sakit',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              SizedBox(height: 40),
              Form(
                key: _loginFormKey,
                child: Column(
                  children: [
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(),
                        ),
                        labelText: 'Username',
                        labelStyle: TextStyle(color: Colors.black87)
                      ),
                      onChanged: (String value) {
                        username = value;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value){
                        if (value == null || value.isEmpty){
                          return "Tolong masukkan Username";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: const BorderSide(),
                        ),
                        labelText: 'Password',
                        labelStyle: TextStyle(color: Colors.black87)
                      ),
                      obscureText: hidePassword,
                      onChanged: (String value) {
                      password = value;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value){
                        if (value == null || value.isEmpty){
                          return "Tolong masukkan Password";
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 20),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Stack(
                        children: <Widget>[
                          Positioned.fill(
                            child: Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    Color(0xFF2B7983),
                                    Color(0xFF35939F),
                                    Color(0xFF3ABA9B),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          TextButton(
                            style: TextButton.styleFrom(
                              padding: const EdgeInsets.all(16.0),
                              shape:RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24.0),
                              ),
                              primary: Colors.white,
                              textStyle: const TextStyle(fontSize: 20),
                            ),
                            onPressed: () async {

                              Map<String,String> head = {
                                "Content-Type": "application/json",
                              };

                              final user = jsonEncode(<String,String>{
                                'username': username,
                                'password': password,
                              });

                              if (_loginFormKey.currentState!.validate()) {
                                final response = await http.post(
                                  Uri.parse("https://pbp-f08.herokuapp.com/acc/loginf"),
                                  headers: head,
                                  body: user,
                                );

                                final resp = jsonDecode(response.body);

                                if(resp['status'] == "not registered"){
                                  showDialog(context: context, builder: (_) => AlertDialog(
                                        title: Text("Username atau Password Salah/Belum Terdaftar",
                                        textAlign: TextAlign.center,),
                                        elevation: 34.0,
                                        backgroundColor: Colors.white,
                                        insetPadding: EdgeInsets.symmetric(horizontal: 50,),
                                      ),
                                    barrierDismissible: true,
                                  );
                                }else if (resp['status'] == "not confirmed"){
                                  showDialog(context: context, builder: (_) => AlertDialog(
                                        title: Text("Rumah Sakit belum terkonfirmasi. Mohon menunggu konfirmasi kami...",
                                        textAlign: TextAlign.center,),
                                        elevation: 34.0,
                                        backgroundColor: Colors.white,
                                        insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                      ),
                                    barrierDismissible: true,
                                  );
                                }else{
                                  SharedPreferences prefs = await SharedPreferences.getInstance();
                                  prefs.setString('username', resp['username']);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => MyApp()),
                                  );
                                  
                                }
                              }

                            },
                            child: const Text('SIGN IN'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}