
import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import '../widgets/display_image_widget.dart';
import 'package:http/http.dart' as http;

class InboxPage extends StatefulWidget {
  const InboxPage({Key? key}) : super(key: key);

  @override
  _InboxPageState createState() => _InboxPageState();
}

class _InboxPageState extends State<InboxPage> {
  TextEditingController _namecontroller = TextEditingController();
  TextEditingController _emailcontroller = TextEditingController();
  TextEditingController _isicontroller = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState

    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF10323A),
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, color: Colors.grey),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFF020215),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: TextButton(
            onPressed: () {},
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center, // use aligment
                  child: Image.asset("assets/images/icon.png",
                      height: 30, width: 30, fit: BoxFit.cover),
                ),
                const SizedBox(
                  width: 10.0,
                ),
                const Text(
                  'HERMITAGE',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ],
            )),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 120, left: 24, right: 24),
        child: Center(
          child: Column(
            children: [
              SizedBox(height: 25),
              Text(
                'Silahkan isi form masukan berikut',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              SizedBox(height: 40),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      validator: (value) {
                        bool emailValid = RegExp(
                                r"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                            .hasMatch(value.toString());
                        if (value == null || value.isEmpty) {
                          return 'Email wajib diisi';
                        } else if (emailValid == false) {
                          return 'Email harus berupa email valid';
                        }
                        // _emailcontroller = value;
                        return null;
                      },
                      controller: _emailcontroller,
                      decoration: InputDecoration(
                          labelText: "Email",
                          border: OutlineInputBorder(),
                          //                         errorBorder: new OutlineInputBorder(
                          //     borderSide: new BorderSide(color: Colors.blue, width: 0.0),
                          // ),
                          errorStyle: TextStyle(color: Colors.white),
                          labelStyle: TextStyle(color: Colors.black),
                          fillColor: Colors.white,
                          filled: true),
                      keyboardType: TextInputType.emailAddress,
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Nama wajib diisi';
                        }
                        // _namecontroller = value;
                        return null;
                      },
                      controller: _namecontroller,
                      decoration: InputDecoration(
                          labelText: "Name",
                          border: OutlineInputBorder(),
                          errorStyle: TextStyle(color: Colors.white),
                          labelStyle: TextStyle(color: Colors.black),
                          fillColor: Colors.white,
                          filled: true),
                      keyboardType: TextInputType.emailAddress,
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      
                      style: TextStyle(color: Colors.black),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Isi wajib diisi';
                        }
                        // _isicontroller = value;
                        return null;
                      },
                      minLines: 5,
                      maxLines: 10,
                      controller: _isicontroller,
                      decoration: InputDecoration(
                          labelText: "isi",
                          border: OutlineInputBorder(),
                          labelStyle: TextStyle(color: Colors.black),
                          errorStyle: TextStyle(color: Colors.white),
                          fillColor: Colors.white,
                          
                          filled: true),
                      keyboardType: TextInputType.emailAddress,
                    ),
                    const SizedBox(height: 20),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Stack(
                        children: <Widget>[
                          Positioned.fill(
                            child: Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    Color(0xFF2B7983),
                                    Color(0xFF35939F),
                                    Color(0xFF3ABA9B),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          
                          ElevatedButton(
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  // If the form is valid, display a snackbar. In the real world,
                                  // you'd often call a server or save the information in a database.
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(content: Text('Terkirim')),
                                  );

                                  setState(() {
                                    tambahMasukan(
                                        _namecontroller.text.toString(),
                                        _emailcontroller.text.toString(),
                                        _isicontroller.text.toString());

                                    _namecontroller.clear();
                                    _emailcontroller.clear();
                                    _isicontroller.clear();
                                  });
                                }
                              },
                              child: Text("Submit"))
                              
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

void tambahMasukan(String nama, String email, String isi) async {
  final response = await http.post(
    Uri.parse('https://pbp-f08.herokuapp.com/masukan/'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body:
        jsonEncode(<String, String>{'nama': nama, 'email': email, 'isi': isi}),
  );
  print(nama);
  print(email);
  print(isi);
  print(response.statusCode);
  print(response.body);
  print(response.request);
}
