// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hermitage/main.dart';

class RegPage extends StatefulWidget {
  const RegPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<RegPage> {
  bool hidePassword = true;
  bool hidePassword2 = true;
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();

  String username = "";
  String password = "";
  String email = "";
  String password1 = "";
  String password2 = "";

  String nama = "";
  String lokasi = "";
  String nomor_telepon = "";
  bool vaksin_ada = false;
  late int vaksin_tersedia;
  bool bed_ada = false;
  late int bed_tersedia;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF10323A),
      appBar: AppBar(
        backgroundColor: Color(0xFF020215),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: TextButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MyApp()),
            );
          },
          child: Row(
            children:[
              Container(
                alignment: Alignment.center,// use aligment
                child: Image.asset("assets/images/icon.png",
                  height: 30,
                  width: 30,
                fit: BoxFit.cover),
              ),
              const SizedBox(
                width: 10.0,
              ),
              const Text(
                'HERMITAGE',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20
                ),
              ),
            ],
          )
        ),
      ),

      body: Padding(
        padding: EdgeInsets.only(top: 10, left: 24, right: 24),
        child: Center(
          child: ListView(
            children: [
              SizedBox(height: 20),
              Text(
                'Informasi Login',
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 30),
              Form(
                key: _loginFormKey,
                child: Column(
                  children: [
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(),
                        ),
                        labelText: 'Username',
                        labelStyle: TextStyle(color: Colors.black87)
                      ),
                      onChanged: (String value) {
                        username = value;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value){
                        if (value == null || value.isEmpty){
                          return "Tolong isi bagian ini";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(),
                        ),
                        labelText: 'Email',
                        labelStyle: TextStyle(color: Colors.black87)
                      ),
                      onChanged: (String value) {
                        email = value;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value){
                        if (value == null || value.isEmpty){
                          return "Tolong isi bagian ini";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: const BorderSide(),
                        ),
                        labelText: 'Password',
                        labelStyle: TextStyle(color: Colors.black87)
                      ),
                      obscureText: hidePassword,
                      onChanged: (String value) {
                      password1 = value;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value){
                        if (value == null || value.isEmpty){
                          return "Tolong isi bagian ini";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(),
                        ),
                        labelText: 'Konfirmasi Password',
                        labelStyle: TextStyle(color: Colors.black87)
                      ),
                      obscureText: hidePassword,
                      onChanged: (String value) {
                        password2 = value;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value){
                        if (value == null || value.isEmpty){
                          return "Tolong isi bagian ini";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 30),
                    Text(
                      'Informasi Rumah Sakit',
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                    SizedBox(height: 30),
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(),
                        ),
                        labelText: 'Nama',
                        labelStyle: TextStyle(color: Colors.black87)
                      ),
                      onChanged: (String value) {
                        nama = value;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value){
                        if (value == null || value.isEmpty){
                          return "Tolong isi bagian ini";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(),
                        ),
                        labelText: 'Lokasi',
                        labelStyle: TextStyle(color: Colors.black87)
                      ),
                      onChanged: (String value) {
                        lokasi = value;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value){
                        if (value == null || value.isEmpty){
                          return "Tolong isi bagian ini";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(),
                        ),
                        labelText: 'Nomor Telepon',
                        labelStyle: TextStyle(color: Colors.black87)
                      ),
                      onChanged: (String value) {
                        nomor_telepon = value;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value){
                        if (value == null || value.isEmpty){
                          return "Tolong isi bagian ini";
                        }else if(RegExp(r'^[a-z]+$').hasMatch(value)){
                          return "Tolong isi menggunakan angka";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        SizedBox(width: 5),
                        Text('Ketersediaan Vaksin',
                          style: TextStyle(fontSize: 17, color: Colors.white70),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(width: 25),
                        Checkbox(
                          fillColor: MaterialStateProperty.all(Colors.white),
                          checkColor: Colors.blueAccent,
                          value: vaksin_ada,
                          onChanged: (bool? value) {
                            setState(() {
                              vaksin_ada = value!;
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(),
                        ),
                        labelText: 'Jumlah Vaksin Tersedia',
                        labelStyle: TextStyle(color: Colors.black87)
                      ),
                      onChanged: (String value) {
                        vaksin_tersedia = int.parse(value);
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value){
                        if (value == null || value.isEmpty){
                          return "Tolong isi bagian ini";
                        }else if(RegExp(r'^[a-z]+$').hasMatch(value)){
                          return "Tolong isi menggunakan angka";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        SizedBox(width: 5),
                        Text('Ketersediaan Bed',
                          style: TextStyle(fontSize: 17, color: Colors.white70),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(width: 47),
                        Checkbox(
                          fillColor: MaterialStateProperty.all(Colors.white),
                          checkColor: Colors.blueAccent,
                          value: bed_ada,
                          onChanged: (bool? value) {
                            setState(() {
                              bed_ada = value!;
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(),
                        ),
                        labelText: 'Jumlah Bed Tersedia',
                        labelStyle: TextStyle(color: Colors.black87)
                      ),
                      onChanged: (String value) {
                        bed_tersedia = int.parse(value);
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value){
                        if (value == null || value.isEmpty){
                          return "Tolong isi bagian ini";
                        }else if(RegExp(r'^[a-z]+$').hasMatch(value)){
                          return "Tolong isi menggunakan angka";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 30),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Stack(
                        children: <Widget>[
                          Positioned.fill(
                            child: Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    Color(0xFF2B7983),
                                    Color(0xFF35939F),
                                    Color(0xFF3ABA9B),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          TextButton(
                            style: TextButton.styleFrom(
                              padding: const EdgeInsets.all(16.0),
                              shape:RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24.0),
                              ),
                              primary: Colors.white,
                              textStyle: const TextStyle(fontSize: 20),
                            ),
                            onPressed: () async {

                              Map<String,String> head = {
                                "Content-Type": "application/json",
                              };

                              final data = jsonEncode(<String, dynamic>{
                                'username': username,
                                'email': email,
                                'password1': password1,
                                'password2': password2,

                                'nama': nama,
                                'lokasi': lokasi,
                                'nomor_telepon': nomor_telepon,
                                'vaksin_ada': vaksin_ada,
                                'vaksin_tersedia': vaksin_tersedia,
                                'bed_ada': bed_ada,
                                'bed_tersedia': bed_tersedia, 

                              });

                              if (_loginFormKey.currentState!.validate()) {
                                final response = await http.post(
                                  Uri.parse("https://pbp-f08.herokuapp.com/acc/regf"),
                                  headers: head,
                                  body: data,
                                );

                                final resp = jsonDecode(response.body);

                                if (resp['status'] == "success"){
                                  showDialog(context: context, builder: (_) => AlertDialog(
                                        title: Text("Register Rumah Sakit Berhasil. Mohon untuk Menunggu Konfirmasi Kami.",
                                        textAlign: TextAlign.center,),
                                        elevation: 34.0,
                                        backgroundColor: Colors.white,
                                        insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                      ),
                                    barrierDismissible: true,
                                  );
                                }else{
                                  showDialog(context: context, builder: (_) => AlertDialog(
                                        title: Text("Terdapat bagian form yang tidak valid. Mohon untuk melakukan pengecekan kembali.",
                                        textAlign: TextAlign.center,),
                                        elevation: 34.0,
                                        backgroundColor: Colors.white,
                                        insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                      ),
                                    barrierDismissible: true,
                                  );
                                }

                              }

                            },
                            child: const Text('REGISTER'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}