// // ignore_for_file: prefer_const_constructors
// import 'package:flutter/material.dart';
// import 'dart:convert';
// import 'package:http/http.dart' as http;
// import 'package:hermitage/detail_rs.dart';
// // import 'package:email_validator/email_validator.dart';

// class EditProfile extends StatefulWidget {
//   @override
//   _EditProfileState createState() => _EditProfileState();
// }

// class _EditProfileState extends State<EditProfile> {
//   TextEditingController _namacontroller = TextEditingController();
//   TextEditingController _alamatcontroller = TextEditingController();
//   TextEditingController _phonecontroller = TextEditingController();
//   TextEditingController _jumlahvaksincontroller = TextEditingController();
//   TextEditingController _jumlahbedcontroller = TextEditingController();
//   var namavalidate;
//   var alamatvalidate;
//   var phonevalidate;
//   bool statusnamavalidate = false;
//   bool statusalamatvalidate = false;
//   bool statusphonevalidate = false;

//   List listRs = [];
//   List primarykeys = [];
//   void getRs() async {
//     listRs.clear();
//     String url = 'https://pbp-f08.herokuapp.com/rumahsakit/';
//     var response = await http.get(Uri.parse(url));
//     List responseBody = jsonDecode(response.body);

//     for (var itemBody in responseBody) {
//       if (itemBody['vaksin_ada']) {
//         listRs.add(itemBody);
//         primarykeys.add(itemBody['id']);
//       }
//     }
//     setState(() {});
//   }

//   @override
//   void initState() {
//     // TODO: implement initState
//     getRs();
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         backgroundColor: Color(0xFF10323A),
//         body: SingleChildScrollView(
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Container(
//                 color: Colors.teal[900],
//                 width: MediaQuery.of(context).size.width,
//                 child: Padding(
//                   padding: const EdgeInsets.all(18),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Text(
//                         "Edit Profile",
//                         style: TextStyle(
//                             fontWeight: FontWeight.w900,
//                             fontSize: 20,
//                             color: Colors.white),
//                       ),
//                       SizedBox(
//                         width: 200,
//                         child: TextField(
//                           style: TextStyle(color: Colors.black),
//                           controller: _namacontroller,
//                           onChanged: (value) {
//                             setState(() {
//                               namavalidate = value;
//                               if (namavalidate.toString().isNotEmpty) {
//                                 statusnamavalidate = true;
//                               }
//                             });
//                           },
//                           decoration: InputDecoration(
//                               labelText: "Nama",
//                               border: OutlineInputBorder(),
//                               labelStyle: TextStyle(color: Colors.black),
//                               fillColor: Colors.white,
//                               filled: true),
//                           keyboardType: TextInputType.name,
//                         ),
//                       ),
//                       SizedBox(
//                         height: 10.0,
//                       ),
//                       SizedBox(
//                         width: 200,
//                         child: TextField(
//                           style: TextStyle(color: Colors.black),
//                           controller: _alamatcontroller,
//                           decoration: InputDecoration(
//                               labelText: "Alamat",
//                               labelStyle: TextStyle(color: Colors.black),
//                               border: OutlineInputBorder(),
//                               fillColor: Colors.white,
//                               filled: true),
//                           keyboardType: TextInputType.text,
//                         ),
//                       ),
//                       SizedBox(
//                         height: 10.0,
//                       ),
//                       SizedBox(
//                         width: 200,
//                         child: TextField(
//                           style: TextStyle(color: Colors.black),
//                           minLines: 5,
//                           maxLines: 10,
//                           controller: _phonecontroller,
//                           decoration: InputDecoration(
//                               labelText: "Nomor Telepon",
//                               labelStyle: TextStyle(color: Colors.black),
//                               border: OutlineInputBorder(),
//                               fillColor: Colors.white,
//                               filled: true),
//                           keyboardType: TextInputType.phone,
//                         ),
//                       ),
//                       const SizedBox(
//                         height: 10.0,
//                       ),
//                       SizedBox(
//                         width: 200,
//                         child: TextField(
//                           style: TextStyle(color: Colors.black),
//                           minLines: 5,
//                           maxLines: 10,
//                           controller: _jumlahvaksincontroller,
//                           decoration: InputDecoration(
//                               labelText: "Jumlah Vaksin",
//                               labelStyle: TextStyle(color: Colors.black),
//                               border: OutlineInputBorder(),
//                               fillColor: Colors.white,
//                               filled: true),
//                           keyboardType: TextInputType.number,
//                         ),
//                       ),
//                       const SizedBox(
//                         height: 10.0,
//                       ),
//                       SizedBox(
//                         width: 200,
//                         child: TextField(
//                           style: TextStyle(color: Colors.black),
//                           minLines: 5,
//                           maxLines: 10,
//                           controller: _jumlahbedcontroller,
//                           decoration: InputDecoration(
//                               labelText: "Jumlah Bed",
//                               labelStyle: TextStyle(color: Colors.black),
//                               border: OutlineInputBorder(),
//                               fillColor: Colors.white,
//                               filled: true),
//                           keyboardType: TextInputType.number,
//                         ),
//                       ),
//                       const SizedBox(
//                         height: 10.0,
//                       ),
//                       ElevatedButton(
//                           onPressed: () async {
//                             if (statusnamavalidate) {
//                               Map<String, String> head = {
//                                 "Content-Type": "application/json",
//                               };

//                               final user = jsonEncode(<String, String>{
//                                 'name': _namacontroller.text,
//                                 'alamat': _alamatcontroller.text,
//                                 'isi': _isicontroller.text
//                               });
//                               // if (_loginFormKey.currentState!.validate()) {
//                               // ignore: unused_local_variable
//                               final response = await http.post(
//                                 Uri.parse(
//                                     "https://pbp-f08.herokuapp.com/masukanmobile/"),
//                                 headers: head,
//                                 body: user,
//                               );
//                             } else {
//                               Null;
//                             }
//                           },
//                           child: Text("Submit"))
//                     ],
//                   ),
//                 ),
//               )
//             ],
//           ),
//         ));
//   }
// }
