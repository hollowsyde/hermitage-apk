import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class ArtikelForm extends StatefulWidget {
  @override
  State<ArtikelForm> createState() => _ArtikelFormState();
}

class _ArtikelFormState extends State<ArtikelForm> {
  TextEditingController _penuliscontroller = TextEditingController();
  TextEditingController _asalrscontroller = TextEditingController();
  TextEditingController _judulcontroller = TextEditingController();
  TextEditingController _isicontroller = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(''), // You can add title here
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, color: Colors.grey),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.black, //You can make this transparent
        elevation: 0.0, //No shadow
      ),
      backgroundColor: Color(0xFF10323A),
      body: SingleChildScrollView(
        padding:
            const EdgeInsets.only(top: 20, left: 24, right: 24, bottom: 30),
        child: Center(
            child: Form(
          key: _formKey,
          child: Column(
            children: [
              const SizedBox(height: 40),
              const Text(
                'Selamat Datang, Pengurus Rumah Sakit',
                style: TextStyle(fontSize: 20),
              ),
              const SizedBox(height: 40),
              const Text(
                "Silakan Tambah Artikel",
                style: TextStyle(fontSize: 20),
              ),
              const SizedBox(height: 40),
              Column(
                children: [
                  TextFormField(
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Harus diisi, ges";
                      }
                      return null;
                    },
                    controller: _penuliscontroller,
                    style: const TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      fillColor: const Color(0xffF1F0F5),
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: const BorderSide(),
                      ),
                      labelText: 'Penulis',
                      labelStyle: const TextStyle(color: Colors.black87),
                      errorStyle: const TextStyle(color: Colors.red),
                    ),
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Harus diisi, ges";
                      }
                      return null;
                    },
                    controller: _asalrscontroller,
                    style: const TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      fillColor: const Color(0xffF1F0F5),
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: const BorderSide(),
                      ),
                      labelText: 'Asal RS',
                      labelStyle: const TextStyle(color: Colors.black87),
                      errorStyle: const TextStyle(color: Colors.red),
                    ),
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Harus diisi, ges";
                      }
                      return null;
                    },
                    controller: _judulcontroller,
                    style: const TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      fillColor: const Color(0xffF1F0F5),
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: const BorderSide(),
                      ),
                      labelText: 'Judul',
                      labelStyle: const TextStyle(color: Colors.black87),
                      errorStyle: const TextStyle(color: Colors.red),
                    ),
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Harus diisi, ges";
                      }
                      return null;
                    },
                    controller: _isicontroller,
                    maxLines: 5,
                    style: const TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      fillColor: const Color(0xffF1F0F5),
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: const BorderSide(),
                      ),
                      labelText: 'Isi',
                      labelStyle: const TextStyle(color: Colors.black87),
                      errorStyle: const TextStyle(color: Colors.red),
                    ),
                  ),
                  const SizedBox(height: 20),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(4),
                    child: Stack(
                      children: <Widget>[
                        Positioned.fill(
                          child: Container(
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                colors: <Color>[
                                  Color(0xFF2B7983),
                                  Color(0xFF35939F),
                                  Color(0xFF3ABA9B),
                                ],
                              ),
                            ),
                          ),
                        ),
                        ElevatedButton(
                          style: TextButton.styleFrom(
                            padding: const EdgeInsets.all(16.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(24.0),
                            ),
                            primary: Colors.white,
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {}
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(content: Text('Terkirim, Asik!')),
                            );

                            setState(() {
                              tambahArtikel(
                                  _penuliscontroller.text.toString(),
                                  _judulcontroller.text.toString(),
                                  _asalrscontroller.text.toString(),
                                  _isicontroller.text.toString());

                              _penuliscontroller.clear();
                              _judulcontroller.clear();
                              _asalrscontroller.clear();
                              _isicontroller.clear();
                            });
                          },
                          child: const Text('Tambah'),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        )),
      ),
    );
  }

  void tambahArtikel(
      String penulis, String judul, String asalRS, String isi) async {
    final response = await http.post(
      Uri.parse('https://pbp-f08.herokuapp.com/article/artikel/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'penulis': penulis,
        'judul': judul,
        'isi': isi,
        'asal_rumah_sakit': asalRS
      }),
    );
    print(penulis);
    print(asalRS);
    print(judul);
    print(isi);
    print(response.statusCode);
    print(response.body);
    print(response.request);
  }
}
