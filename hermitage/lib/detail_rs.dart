import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import '../widgets/display_image_widget.dart';
import 'package:http/http.dart' as http;

//template by awesomeflutter
// This class handles the Page to dispaly the user's info on the "Edit Profile" Screen
class DetailRs extends StatefulWidget {
  final int pk;
  const DetailRs({Key? key, required this.pk}) : super(key: key);

  @override
  _DetailRsState createState() => _DetailRsState();
}

class _DetailRsState extends State<DetailRs> {
  TextEditingController _namecontroller = TextEditingController();
  TextEditingController _emailcontroller = TextEditingController();
  TextEditingController _isicontroller = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  String nama = '';
  String alamat = '';
  String phone = '';
  String jumlahvaksin = '';
  String jumlahbed = '';

  Future<void> fetchRs() async {
    final url = 'https://pbp-f08.herokuapp.com/detail/${widget.pk}/';
    final response = await http.get(Uri.parse(url));
    Map<String, dynamic> extractedData = jsonDecode(response.body);

    nama = extractedData['nama'];
    alamat = extractedData['lokasi'];
    phone = extractedData['nomor_telepon'];
    jumlahvaksin = extractedData['vaksin_tersedia'].toString();
    jumlahbed = extractedData['bed_tersedia'].toString();

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    fetchRs();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF10323A),
      body: SingleChildScrollView(
        child: Column(
          children: [
            AppBar(
              centerTitle: true,
              backgroundColor: Colors.transparent,
              elevation: 0,
              toolbarHeight: 60,
              title: Text('Detail Rumah Sakit',
                  style: TextStyle(fontWeight: FontWeight.bold)),
            ),
            Center(
                child: Padding(
                    padding: EdgeInsets.only(bottom: 20),
                    child: Text(
                      nama,
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ))),
            InkWell(
                child: DisplayImage(
              imagePath: "assets/images/medicine.png",
            )),
            buildUserInfoDisplay(nama, 'Name'),
            buildUserInfoDisplay(phone, 'Phone'),
            buildUserInfoDisplay(alamat, 'Alamat'),
            buildUserInfoDisplay(jumlahbed, 'Jumlah Bed'),
            buildUserInfoDisplay(jumlahvaksin, 'Jumlah Vaksin'),
            const SizedBox(height: 40),
            Container(
              color: Colors.teal[900],
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(18),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Masukan",
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 20,
                            color: Colors.white),
                      ),
                      SizedBox(
                        width: 200,
                        child: TextFormField(
                          style: TextStyle(color: Colors.black),
                          validator: (value) {
                            bool emailValid = RegExp(
                                    //regex dari https://regexr.com/2rhq7
                                    r"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                                .hasMatch(value.toString());
                            if (value == null || value.isEmpty) {
                              return 'Email wajib diisi';
                            } else if (emailValid == false) {
                              return 'Email harus berupa email valid';
                            }
                            // _emailcontroller = value;
                            return null;
                          },
                          controller: _emailcontroller,
                          decoration: InputDecoration(
                              labelText: "Email",
                              border: OutlineInputBorder(),
                              errorStyle: TextStyle(color: Colors.white),
                              labelStyle: TextStyle(color: Colors.black),
                              fillColor: Colors.white,
                              filled: true),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      SizedBox(
                        width: 200,
                        child: TextFormField(
                          style: TextStyle(color: Colors.black),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Nama wajib diisi';
                            }
                            return null;
                          },
                          controller: _namecontroller,
                          decoration: InputDecoration(
                              labelText: "Name",
                              border: OutlineInputBorder(),
                              errorStyle: TextStyle(color: Colors.white),
                              labelStyle: TextStyle(color: Colors.black),
                              fillColor: Colors.white,
                              filled: true),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      SizedBox(
                        width: 200,
                        child: TextFormField(
                          style: TextStyle(color: Colors.black),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Isi wajib diisi';
                            }
                            return null;
                          },
                          minLines: 5,
                          maxLines: 10,
                          controller: _isicontroller,
                          decoration: InputDecoration(
                              labelText: "isi",
                              border: OutlineInputBorder(),
                              labelStyle: TextStyle(color: Colors.black),
                              errorStyle: TextStyle(color: Colors.white),
                              fillColor: Colors.white,
                              filled: true),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(content: Text('Terkirim')),
                              );

                              setState(() {
                                tambahMasukan(
                                    _namecontroller.text.toString(),
                                    _emailcontroller.text.toString(),
                                    _isicontroller.text.toString());

                                _namecontroller.clear();
                                _emailcontroller.clear();
                                _isicontroller.clear();
                              });
                            }
                          },
                          child: Text("Submit"))
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  // Widget builds the display item with the proper formatting to display the user's info, (sumber dari awesomeflutter)
  Widget buildUserInfoDisplay(String getValue, String title) => Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w500,
              color: Colors.grey,
            ),
          ),
          SizedBox(
            height: 1,
          ),
          Container(
              width: 350,
              height: 40,
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                color: Colors.grey,
                width: 1,
              ))),
              child: Row(children: [
                Expanded(
                    child: Text(
                  getValue,
                  style: TextStyle(fontSize: 16, height: 1.4),
                )),
                Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.grey,
                  size: 40.0,
                )
              ]))
        ],
      ));

  // Refrshes the Page after updating user info.
  void tambahMasukan(String nama, String email, String isi) async {
    final response = await http.post(
      Uri.parse('https://pbp-f08.herokuapp.com/masukan/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
          <String, String>{'nama': nama, 'email': email, 'isi': isi}),
    );
    print(nama);
    print(email);
    print(isi);
    print(response.statusCode);
    print(response.body);
    print(response.request);
  }
}
