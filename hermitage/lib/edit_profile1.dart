import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import '../widgets/display_image_widget.dart';
import 'package:http/http.dart' as http;

class EditProfile1 extends StatefulWidget {
  final int id; //tambahan, jadi ini kan di pass dari profile_user kan
  const EditProfile1({Key? key, required this.id}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile1> {
  TextEditingController _namacontroller = TextEditingController();
  TextEditingController _alamatcontroller = TextEditingController();
  TextEditingController _phonecontroller = TextEditingController();
  TextEditingController _jumlahvaksincontroller = TextEditingController();
  TextEditingController _jumlahbedcontroller = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  //ambil inisiasi data dari django

  // String nama = '';
  // String alamat = '';
  // String phone = '';
  // String jumlahvaksin = 0;
  // String jumlahbed = 0;

  Future<void> fetchRs() async {
    final url = 'https://pbp-f08.herokuapp.com/rumahsakit/${widget.id}/'; //
    final response = await http.get(Uri.parse(url));
    Map<String, dynamic> extractedData = jsonDecode(response.body);
    // rs = extractedData['nama'];

    _namacontroller.text = extractedData['nama'];
    _alamatcontroller.text = extractedData['lokasi'];
    _phonecontroller.text = extractedData['nomor_telepon'];
    _jumlahvaksincontroller.text = extractedData['vaksin_tersedia'].toString();
    _jumlahbedcontroller.text = extractedData['bed_tersedia'].toString();
    print(extractedData['nama']);

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchRs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF10323A),
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, color: Colors.grey),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFF020215),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: TextButton(
            onPressed: () {},
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center, // use aligment
                  child: Image.asset("assets/images/icon.png",
                      height: 30, width: 30, fit: BoxFit.cover),
                ),
                const SizedBox(
                  width: 10.0,
                ),
                const Text(
                  'HERMITAGE',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ],
            )),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 120, left: 24, right: 24),
          child: Center(
            child: Column(
              children: [
                SizedBox(height: 25),
                Text(
                  'Edit Profil Rumah Sakit',
                  style: TextStyle(fontSize: 50, color: Colors.white),
                ),
                SizedBox(height: 40),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Nama wajib diisi';
                          }
                          return null;
                        },
                        controller: _namacontroller,
                        decoration: InputDecoration(
                            labelText: "Nama",
                            border: OutlineInputBorder(),
                            errorStyle: TextStyle(color: Colors.black87),
                            labelStyle:
                                TextStyle(color: Colors.white, fontSize: 20),
                            fillColor: Colors.black87,
                            filled: true),
                        keyboardType: TextInputType.name,
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Alamat wajib diisi';
                          }
                          return null;
                        },
                        minLines: 5,
                        maxLines: 10,
                        controller: _alamatcontroller,
                        decoration: InputDecoration(
                            labelText: "Alamat",
                            border: OutlineInputBorder(),
                            labelStyle:
                                TextStyle(color: Colors.white, fontSize: 20),
                            errorStyle: TextStyle(color: Colors.black87),
                            fillColor: Colors.black87,
                            filled: true),
                        keyboardType: TextInputType.text,
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Nomor telepon wajib diisi';
                          }
                          return null;
                        },
                        minLines: 1,
                        maxLines: 10,
                        controller: _phonecontroller,
                        decoration: InputDecoration(
                            labelText: "Nomor Telepon",
                            border: OutlineInputBorder(),
                            labelStyle:
                                TextStyle(color: Colors.white, fontSize: 20),
                            errorStyle: TextStyle(color: Colors.black87),
                            fillColor: Colors.black87,
                            filled: true),
                        keyboardType: TextInputType.phone,
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Jika tidak ada, tulis 0';
                          }
                          return null;
                        },
                        minLines: 1,
                        maxLines: 10,
                        controller: _jumlahvaksincontroller,
                        decoration: InputDecoration(
                            labelText: "Jumlah Vaksin Tersedia",
                            border: OutlineInputBorder(),
                            labelStyle:
                                TextStyle(color: Colors.white, fontSize: 20),
                            errorStyle: TextStyle(color: Colors.black87),
                            fillColor: Colors.black87,
                            filled: true),
                        keyboardType: TextInputType.number,
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Jika tidak ada, tulis 0';
                          }
                          // _isicontroller = value;
                          return null;
                        },
                        minLines: 1,
                        maxLines: 10,
                        controller: _jumlahbedcontroller,
                        decoration: InputDecoration(
                            labelText: "Jumlah Bed Tersedia",
                            border: OutlineInputBorder(),
                            labelStyle:
                                TextStyle(color: Colors.white, fontSize: 20),
                            errorStyle: TextStyle(color: Colors.black87),
                            fillColor: Colors.black87,
                            filled: true),
                        keyboardType: TextInputType.number,
                      ),
                      const SizedBox(height: 20),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(4),
                        child: Stack(
                          children: <Widget>[
                            Positioned.fill(
                              child: Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: <Color>[
                                      Color(0xFF2B7983),
                                      Color(0xFF35939F),
                                      Color(0xFF3ABA9B),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    // If the form is valid, display a snackbar. In the real world,
                                    // you'd often call a server or save the information in a database.
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                          content:
                                              Text('Berhasil Update Profil')),
                                    );

                                    setState(() {
                                      tambahMasukan(
                                          _namacontroller.text.toString(),
                                          _alamatcontroller.text.toString(),
                                          _phonecontroller.text.toString(),
                                          int.parse(_jumlahvaksincontroller
                                              .text), //ini diparse jadi int
                                          int.parse(_jumlahbedcontroller.text),
                                          widget.id);

                                      // _namecontroller.clear();
                                      // _emailcontroller.clear();
                                      // _isicontroller.clear();
                                    });
                                  }
                                },
                                child: Text("Update Profil"))
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

void tambahMasukan(String nama, String lokasi, String nomortelepon,
    int jumlahVaksin, int jumlahBed, int key) async {
  final response = await http.put(
    Uri.parse('https://pbp-f08.herokuapp.com/rumahsakit/${key}/'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, dynamic>{
      'nama': nama,
      'lokasi': lokasi,
      'nomor_telepon': nomortelepon,
      'bed_tersedia': jumlahBed,
      'vaksin_tersedia': jumlahVaksin,
    }),
  );
  // print(nama);
  // print(email);
  // print(isi);
  print(response.statusCode);
  print(response.body);
  print(response.request);
}
