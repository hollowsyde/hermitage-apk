// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

import 'pages/reg.dart';

void main() {
  runApp(RegApp());
}

class RegApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hermitage',
      home: RegPage(),
    );
  }
}