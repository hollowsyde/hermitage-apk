// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:hermitage/detail_rs.dart';
import 'package:email_validator/email_validator.dart';

class VaksinPage extends StatefulWidget {
  @override
  State<VaksinPage> createState() => _VaksinPageState();
}

class _VaksinPageState extends State<VaksinPage> {
  TextEditingController _namecontroller = TextEditingController();
  TextEditingController _emailcontroller = TextEditingController();
  TextEditingController _isicontroller = TextEditingController();
  var emailvalidate;
  bool statusemailvalidate = false;

  List listRs = [];
  List primarykeys = [];
  void getRs() async {
    listRs.clear();
    String url = 'https://pbp-f08.herokuapp.com/rumahsakit/';
    var response = await http.get(Uri.parse(url));
    List responseBody = jsonDecode(response.body);

    for (var itemBody in responseBody) {
      if (itemBody['vaksin_ada']) {
        listRs.add(itemBody);
        primarykeys.add(itemBody['id']);
      }
    }
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    getRs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF10323A),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Selamat Datang,'),
              listRs.isEmpty
                  ? const Text("Tidak ada.")
                  : GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2, childAspectRatio: 1),
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: listRs.length,
                      itemBuilder: (context, index) {
                        Map<String, dynamic> itemRs = listRs[index];
                        return Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8),
                            child: Column(
                              children: [
                                Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Text(
                                      itemRs['nama'],
                                    )),
                                Expanded(
                                  child: Image(
                                    image:
                                        AssetImage('assets/images/vaccine.png'),
                                    fit: BoxFit.cover,
                                    height: double.maxFinite,
                                    width: double.maxFinite,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: ElevatedButton(
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => DetailRs(
                                                pk: primarykeys[index])),
                                      );
                                    },
                                    child: Text("Detail"),
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      }),
              Container(
                color: Colors.teal[900],
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(18),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Masukan",
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 20,
                            color: Colors.white),
                      ),
                      SizedBox(
                        width: 200,
                        child: TextField(
                          style: TextStyle(color: Colors.black),
                          controller: _emailcontroller,
                          onChanged: (value) {
                            setState(() {
                              emailvalidate = value;
                              statusemailvalidate =
                                  EmailValidator.validate(emailvalidate);
                            });
                          },
                          decoration: InputDecoration(
                              labelText: "Email",
                              border: OutlineInputBorder(),
                              labelStyle: TextStyle(color: Colors.black),
                              fillColor: Colors.white,
                              filled: true),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      SizedBox(
                        width: 200,
                        child: TextField(
                          style: TextStyle(color: Colors.black),
                          controller: _namecontroller,
                          decoration: InputDecoration(
                              labelText: "Name",
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(),
                              fillColor: Colors.white,
                              filled: true),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      SizedBox(
                        width: 200,
                        child: TextField(
                          style: TextStyle(color: Colors.black),
                          minLines: 5,
                          maxLines: 10,
                          controller: _isicontroller,
                          decoration: InputDecoration(
                              labelText: "isi",
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(),
                              fillColor: Colors.white,
                              filled: true),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      ElevatedButton(
                          onPressed: () async {
                            if (statusemailvalidate) {
                              Map<String, String> head = {
                                "Content-Type": "application/json",
                              };

                              final user = jsonEncode(<String, String>{
                                'name': _namecontroller.text,
                                'email': _emailcontroller.text,
                                'isi': _isicontroller.text
                              });
                              // if (_loginFormKey.currentState!.validate()) {
                              // ignore: unused_local_variable
                              final response = await http.post(
                                Uri.parse(
                                    "https://pbp-f08.herokuapp.com/masukanmobile/"),
                                headers: head,
                                body: user,
                              );
                            } else {
                              Null;
                            }
                          },
                          child: Text("Submit"))
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
