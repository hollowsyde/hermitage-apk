import 'package:flutter/material.dart';
// flutter run --no-sound-null-safety

// more
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:hermitage/profile_user.dart';
import 'artikel.dart';
import 'vaksin.dart';
import 'bed.dart';
import 'home.dart';
import 'main_log.dart';
import 'daftarArtikel.dart';

import 'pages/inbox.dart';
import 'main_reg.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF10323A),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 2,
            child: Container(
              child: Stack(
                children: <Widget>[
                  Positioned(
                    bottom: 23,
                    top: 0,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.elliptical(30, 30),
                          bottomRight: Radius.elliptical(30, 30),
                        ),
                        /*gradient: LinearGradient(
                              colors: [
                                Color(0xFF10323A),
                                Color(0xFF10323A),
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight),*/
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          //Icon(Icons.menu, color: Colors.white),
                          Spacer(),
                          RichText(
                            text: TextSpan(
                              style: TextStyle(letterSpacing: 1.3),
                              children: [
                                TextSpan(
                                  text: "Welcome",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 25,
                                    color: Colors.white,
                                  ),
                                ),
                                TextSpan(
                                  text: " to",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 25,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 9,
                          ),
                          Text(
                            "HERMITAGE",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 25,
                              letterSpacing: 1.3,
                            ),
                          ),
                          //Spacer(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Flexible(
            flex: 5,
            child: Container(
              padding: EdgeInsets.all(15),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    CategoryContainer(),
                    SizedBox(
                      height: 9,
                    ),
                    Divider(),
                    SizedBox(
                      height: 9,
                    ),
                    DealsContainer(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CategoryContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 9,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                child: Column(
                  children: <Widget>[
                    IconButton(
                      icon: Image.asset("assets/images/profile.png"),
                      iconSize: 70,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfileApp(
                                    pk: 1,
                                  )),
                        );
                      },
                    ),
                    SizedBox(height: 10),
                    Text("Profil")
                  ],
                ),
              ),
              Flexible(
                child: Column(
                  children: <Widget>[
                    IconButton(
                      icon: Image.asset("assets/images/login.png"),
                      iconSize: 70,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => LoginApp()),
                        );
                      },
                    ),
                    SizedBox(height: 10),
                    Text("Login")
                  ],
                ),
              ),
              Flexible(
                child: Column(
                  children: <Widget>[
                    IconButton(
                      icon: Image.asset("assets/images/register.png"),
                      iconSize: 70,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => RegApp()),
                        );
                      },
                    ),
                    SizedBox(height: 10),
                    Text("Register")
                  ],
                ),
              ),
              Flexible(
                child: Column(
                  children: <Widget>[
                    IconButton(
                      icon: Image.asset("assets/images/newspaper.png"),
                      iconSize: 70,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => InboxPage()),
                        );
                      },
                    ),
                    SizedBox(height: 10),
                    Text("Masukan")
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class DealsContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/details');
      },
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "More Information",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
                ),
                Spacer(),
              ],
            ),
            SizedBox(
              height: 9,
            ),
            Container(
              height: 171,
              child: ListView.builder(
                itemCount: 1,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, id) {
                  return SingleItem();
                },
              ),
            ),
            Container(
              height: 171,
              child: ListView.builder(
                itemCount: 1,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, id) {
                  return SecondItem();
                },
              ),
            ),
            Container(
              height: 171,
              child: ListView.builder(
                itemCount: 1,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, id) {
                  return ThirdItem();
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

class SingleItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 251,
      margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(color: Colors.grey, blurRadius: 3.0),
        ],
      ),
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 0,
            right: 0,
            child: Container(
              child: Image.asset(
                "assets/images/vax.png",
                width: 140,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Silahkan cek",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black.withOpacity(1.0)),
                ),
                Text(
                  "VAKSIN",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.blue.withOpacity(1.0),
                      fontSize: 22),
                ),
                Text(
                  "yang tersedia \npada tab vaksin",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF10323A),
                      fontSize: 12),
                ),
                Spacer(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class SecondItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 251,
      margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(color: Colors.grey, blurRadius: 3.0),
        ],
      ),
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 2,
            right: 5,
            child: Container(
              child: Image.asset(
                "assets/images/bedd.jpg",
                width: 200,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Silahkan cek",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black.withOpacity(1.0)),
                ),
                Text(
                  "BED",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.blue.withOpacity(1.0),
                      fontSize: 25),
                ),
                Text(
                  "yang\ntersedia \npada\ntab bed",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF10323A),
                      fontSize: 12),
                ),
                Spacer(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ThirdItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 251,
      margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(color: Colors.grey, blurRadius: 3.0),
        ],
      ),
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 2,
            right: 5,
            child: Container(
              child: Image.asset(
                "assets/images/art.jpg",
                width: 200,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Silahkan cek",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black.withOpacity(1.0)),
                ),
                Text(
                  "ARTIKEL",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.blue.withOpacity(1.0),
                      fontSize: 25),
                ),
                Text(
                  "yang\ntersedia \npada\ntab artikel",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF10323A),
                      fontSize: 12),
                ),
                Spacer(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

/*
Template : https://cybdom.tech/furniture-app-ui-in-flutter-source-code/
*/

// TODO: Register!