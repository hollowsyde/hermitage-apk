// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

import 'pages/log.dart';

void main() {
  runApp(LoginApp());
}

class LoginApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hermitage',
      home: LoginPage(),
    );
  }
}
