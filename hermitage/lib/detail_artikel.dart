import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:hermitage/daftarArtikel.dart';
import '../widgets/display_image_widget.dart';
import 'package:http/http.dart' as http;

// This class handles the Page to dispaly the user's info on the "Edit Profile" Screen
class DetailArtikel extends StatefulWidget {
  final int pk;
  const DetailArtikel({Key? key, required this.pk}) : super(key: key);

  @override
  _DetailArtikelState createState() => _DetailArtikelState();
}

class _DetailArtikelState extends State<DetailArtikel> {
  // late RumahSakit data;
  String penulis = '';
  String asalRS = '';
  String judul = '';
  String tanggalPembuatan = '';
  String isi = '';

  Future<void> fetchArtikel() async {
    final url = 'https://pbp-f08.herokuapp.com/article/artikel/${widget.pk}/';
    final response = await http.get(Uri.parse(url));
    Map<String, dynamic> extractedData = jsonDecode(response.body);
    // rs = extractedData['nama'];

    penulis = extractedData['penulis'];
    asalRS = extractedData['asal_rumah_sakit'];
    judul = extractedData['judul'];
    tanggalPembuatan = extractedData['tanggal_pembuatan'].toString();
    isi = extractedData['isi'];

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    fetchArtikel();
  }

  @override
  Widget build(BuildContext context) {
    // final user = UserData.myUser;

    return Scaffold(
        backgroundColor: Color(0xFF10323A),
        body: SingleChildScrollView(
          child: Column(
            children: [
              AppBar(
                centerTitle: true,
                backgroundColor: Colors.transparent,
                elevation: 0,
                toolbarHeight: 60,
                title: Text('Artikel',
                    style: TextStyle(fontWeight: FontWeight.bold)),
              ),
              Center(
                  child: Padding(
                      padding: EdgeInsets.only(
                          bottom: 20, right: 40, left: 40, top: 20),
                      child: Text(
                        judul,
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ))),
              InkWell(
                  // onTap: () {
                  //   navigateSecondPage(EditImagePage());
                  // },
                  child: DisplayImage(
                imagePath: "assets/images/newspaper.png",
                // onPressed: () {},
              )),
              buildUserInfoDisplay(penulis, 'Penulis'),
              buildUserInfoDisplay(asalRS, 'Asal RS'),
              buildUserInfoDisplay(tanggalPembuatan, 'Tanggal Pembuatan'),
              buildUserInfoDisplay2(isi, 'Isi'),
              // Expanded(
              //   child: buildAbout(user),
              //   flex: 4,
              // )
            ],
          ),
        ));
  }

  // Widget builds the display item with the proper formatting to display the user's info
  Widget buildUserInfoDisplay(String getValue, String title) => Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w500,
              color: Colors.grey,
            ),
          ),
          SizedBox(
            height: 1,
          ),
          Container(
              width: 350,
              height: 40,
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                color: Colors.grey,
                width: 1,
              ))),
              child: Row(children: [
                Expanded(
                    child: Text(
                  getValue,
                  style: TextStyle(fontSize: 16, height: 1.4),
                )),
                Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.grey,
                  size: 40.0,
                )
              ]))
        ],
      ));

  Widget buildUserInfoDisplay2(String getValue, String title) => Padding(
      padding: EdgeInsets.only(bottom: 30, left: 30, right: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w500,
              color: Colors.grey,
            ),
          ),
          SizedBox(
            height: 1,
          ),
          Container(
              width: 700,
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                color: Colors.grey,
                width: 1,
              ))),
              child: Row(children: [
                Expanded(
                    child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Text(
                          isi,
                          style: TextStyle(
                            fontSize: 16,
                            height: 1.4,
                          ),
                        ))),
              ]))
        ],
      ));

  // Refrshes the Page after updating user info.
  FutureOr onGoBack(dynamic value) {
    setState(() {});
  }

  // Handles navigation and prompts refresh.
  void navigateSecondPage(Widget editForm) {
    Route route = MaterialPageRoute(builder: (context) => editForm);
    Navigator.push(context, route).then(onGoBack);
  }
}
