import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:hermitage/profile_user.dart';
import 'artikel.dart';
import 'vaksin.dart';
import 'bed.dart';
import 'home.dart';
import 'main_log.dart';
import 'main_reg.dart';
import 'daftarArtikel.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hermitage',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(title: 'HERMITAGE'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  int _counter = 0;
  int _currentIndex = 0;
  static String _username = "";
  static List<Widget> _halaman = <Widget>[
    HomePage(),
    BedPage(),
    VaksinPage(),
    ArtikelDaftar(),
    ProfileApp(
      pk: 1,
    ),
  ];

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  void _getUsername() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _username = prefs.getString('username') ?? "";
    });
  }

  @override
  Widget build(BuildContext context) {
    _getUsername();
    return Scaffold(
      drawer: SideDrawer(),
      backgroundColor: Color(0xFF10323A),
      appBar: AppBar(
        backgroundColor: Color(0xFF020215),
        title: TextButton(
            onPressed: () {},
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center, // use aligment
                  child: Image.asset("assets/images/icon.png",
                      height: 30, width: 30, fit: BoxFit.cover),
                ),
                const SizedBox(
                  width: 10.0,
                ),
                const Text(
                  'HERMITAGE',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ],
            )),
      ),
      body: _halaman[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.house),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.airline_seat_individual_suite),
            label: 'Bed',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment),
            label: 'Vaksin',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: 'Artikel',
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.corporate_fare_rounded),
          //   label: 'Profil',
          // ),
        ],
      ),
    );
  }
}

class SideDrawer extends StatefulWidget {
  const SideDrawer({Key? key}) : super(key: key);

  @override
  _SideDrawer createState() => _SideDrawer();
}

class _SideDrawer extends State<SideDrawer> {
  bool regVis = true;

  String _cek(test) {
    if (test == "") {
      return 'Selamat Datang, Pengunjung.';
    } else {
      setState(() {
        regVis = false;
      });
      return 'Selamat Datang, ' + test;
    }
  }

  String _cek2(test) {
    if (test == "") {
      return 'Login';
    } else {
      return 'Logout';
    }
  }

  void _logout(context) async {
    final response = await http.post(
      Uri.parse("https://pbp-f08.herokuapp.com/acc/logoutf"),
    );

    final resp = jsonDecode(response.body);

    SharedPreferences preferences = await SharedPreferences.getInstance();

    if (resp['status'] == "logout") {
      await preferences.clear();
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => MyApp()),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          DrawerHeader(
            child: Center(
              child: Text(
                _cek(_MyHomePageState._username),
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            decoration: BoxDecoration(
              color: Color(0xFF10323A),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.corporate_fare_rounded),
            title: Text('Profile'),
            onTap: () => {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ProfileApp(pk: 1)))
            },
          ),
          Visibility(
            visible: regVis,
            child: ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Register'),
              onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => RegApp()),
                        )
            ),
          ),
          ListTile(
              leading: Icon(Icons.login_sharp),
              title: Text(_cek2(_MyHomePageState._username)),
              onTap: () => {
                    if (_cek2(_MyHomePageState._username) == 'Login')
                      {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => LoginApp()),
                        )
                      }
                    else
                      {_logout(context)}
                  }),
        ],
      ),
    );
  }
}
