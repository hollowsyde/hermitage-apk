// import 'package:flutter/material.dart';
// import '../widgets/display_image_widget.dart';

// class ProfileUser extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Color(0xFF10323A),
//       body: Column(
//         children: [
//           AppBar(
//             backgroundColor: Colors.black54,
//             elevation: 0,
//             toolbarHeight: 16,
//           )

//         ],
//       )
//     );
//   }
// }
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'edit_profile1.dart';

class ProfileApp extends StatefulWidget {
  final int pk;
  const ProfileApp({Key? key, required this.pk}) : super(key: key);

  @override
  _ProfileAppState createState() => _ProfileAppState();
}

class _ProfileAppState extends State<ProfileApp> {
  String nama = '';
  String alamat = '';
  String phone = '';
  String jumlahvaksin = '';
  String jumlahbed = '';
  // static List<Widget> _halaman = <Widget>[EditProfile1(id: widget.pk)];

  Future<void> fetchRs() async {
    final url = 'https://pbp-f08.herokuapp.com/rumahsakit/${widget.pk}/';
    final response = await http.get(Uri.parse(url));
    Map<String, dynamic> extractedData = jsonDecode(response.body);

    nama = extractedData['nama'];
    alamat = extractedData['lokasi'];
    phone = extractedData['nomor_telepon'];
    jumlahvaksin = extractedData['vaksin_tersedia'].toString();
    jumlahbed = extractedData['bed_tersedia'].toString();

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchRs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, color: Colors.grey),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFF020215),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: TextButton(
            onPressed: () {},
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center, // use aligment
                  child: Image.asset("assets/images/icon.png",
                      height: 30, width: 30, fit: BoxFit.cover),
                ),
                const SizedBox(
                  width: 10.0,
                ),
                const Text(
                  'HERMITAGE',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ],
            )),
      ),
      body: Column(
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Color(0xFF10323A), Colors.black54])),
              child: Container(
                width: double.infinity,
                height: 350.0,
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                          "https://images.unsplash.com/photo-1626315869436-d6781ba69d6e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80",
                        ),
                        radius: 50.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        nama,
                        style: TextStyle(
                          fontSize: 22.0,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Card(
                        margin: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 5.0),
                        clipBehavior: Clip.antiAlias,
                        color: Colors.white,
                        elevation: 5.0,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 8.0, vertical: 22.0),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Vaksin Tersedia",
                                      style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: 22.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Text(
                                      jumlahvaksin,
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.black87,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Bed Tersedia",
                                      style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: 22.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Text(
                                      jumlahbed,
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.black87,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              // Expanded(
                              //   child: Column(
                              //     children: <Widget>[
                              //       Text(
                              //         "Follow",
                              //         style: TextStyle(
                              //           color: Colors.redAccent,
                              //           fontSize: 22.0,
                              //           fontWeight: FontWeight.bold,
                              //         ),
                              //       ),
                              //       SizedBox(
                              //         height: 5.0,
                              //       ),
                              //       Text(
                              //         "1300",
                              //         style: TextStyle(
                              //           fontSize: 20.0,
                              //           color: Colors.pinkAccent,
                              //         ),
                              //       )
                              //     ],
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )),
          Container(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 30.0, horizontal: 16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Keterangan",
                    style: TextStyle(
                        color: Colors.white,
                        fontStyle: FontStyle.normal,
                        fontSize: 28.0),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    'Lokasi:  ${alamat}\n'
                    'Nomor telepon: ${phone}',
                    style: TextStyle(
                      fontSize: 22.0,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w300,
                      color: Colors.white,
                      letterSpacing: 2.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Container(
            width: 300.00,
            child: RaisedButton(
                onPressed: () => {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditProfile1(
                                  id: widget.pk))) //ini tambahan pk
                    },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80.0)),
                elevation: 0.0,
                padding: EdgeInsets.all(0.0),
                child: Ink(
                  decoration: BoxDecoration(
                    color: Color(0xFF10323A),
                    // gradient: LinearGradient(
                    //     begin: Alignment.centerRight,
                    //     end: Alignment.centerLeft,
                    //     colors: [Colors.redAccent, Colors.pinkAccent]),
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  child: Container(
                    constraints:
                        BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                    alignment: Alignment.center,
                    child: Text(
                      "Edit Profil",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 26.0,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
