import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:hermitage/artikel.dart';
import 'package:hermitage/detail_artikel.dart';
import 'package:http/http.dart' as http;

class ArtikelDaftar extends StatefulWidget {
  @override
  State<ArtikelDaftar> createState() => _ArtikelPageState();
}

class _ArtikelPageState extends State<ArtikelDaftar> {
  List listArtikel = [];
  List primaryKeys = [];
  void getArtikel() async {
    listArtikel.clear();
    String url = 'https://pbp-f08.herokuapp.com/article/artikel/';
    var response = await http.get(Uri.parse(url));
    List responseBody = jsonDecode(response.body);

    for (var itemBody in responseBody) {
      listArtikel.add(itemBody);
      primaryKeys.add(itemBody['id']);
    }
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    getArtikel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF10323A),
        body: SingleChildScrollView(
            padding: const EdgeInsets.only(top: 20, left: 24, right: 24),
            child: Center(
                child: Column(children: [
              const SizedBox(height: 40),
              const Text(
                'Selamat Datang, Pengunjung',
                style: TextStyle(fontSize: 30),
              ),
              const SizedBox(height: 40),
              const Text('Silakan lihat daftar artikel kami',
                  style: TextStyle(fontSize: 20)),
              const SizedBox(height: 20),
              const Text(
                  "Jika Anda Admin RS, Anda dapat menambahkan artikel melalui tombol ini"),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () => {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ArtikelForm())),
                },
                child: const Text('Tambah', style: TextStyle(fontSize: 18)),
              ),
              const SizedBox(height: 20),
              GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, childAspectRatio: 1),
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: listArtikel.length,
                  itemBuilder: (context, index) {
                    Map<String, dynamic> itemArtikel = listArtikel[index];
                    return Container(
                        height: 500,
                        width: 200,
                        child: Card(
                            child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: Column(children: [
                                  Padding(
                                      padding: EdgeInsets.all(10.0),
                                      child: Text(itemArtikel['judul'],
                                          style: TextStyle(fontSize: 18))),
                                  Expanded(
                                    child: Image(
                                      image: AssetImage(
                                          'assets/images/gambarArtikel.png'),
                                      fit: BoxFit.cover,
                                      height: double.maxFinite,
                                      width: double.maxFinite,
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: ElevatedButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailArtikel(
                                                      pk: primaryKeys[index])),
                                        );
                                      },
                                      child: Text("Detail"),
                                    ),
                                  )
                                ]))));
                  })
            ]))));
  }
}
