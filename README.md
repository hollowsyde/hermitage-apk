# Kelompok F08
                   
| Nama | NPM |
| ------ | ------ |
| Alfatih Aditya Susanto | 2006596402 |
| Abel Ganindra | 2006597090 |
| Immanuel Brilan Solvanto Darmawan | 2006596674 |
| Abdul Ghani Suryo Adhipramono  | 2006596970  |
| Agnes Audya Tiara P  | 2006596200  |
| Avelia Diva Zahra    | 2006596176 |

### Integrasi dengan Web Sebelumnya


**1.  Autentikasi**

| Fitur | Cerita |
| ------ | ------ |
| _Login_ | Fitur _**Login**_ digunakan untuk mengautentikasi user yang sudah _register_ dan sudah _confirmed_ oleh admin menggunakan _back-end_ django dari _project_ tengah semester. Integrasi dengan _web service_ pada _project_ tengah semester dilakukan menggunakan _package_ http pada flutter. |
| _Register_ | Fitur _**Register**_ akan menambahkan user baru ke dalam *database*. Method yang akan digunakan adalah method **POST**. Implementasi metode akan memanfaatkan Firebase Auth REST API dan menggunakan link https://identitytoolkit.googleapis.com/v1/accounts:signInWithCustomToken?key=[API_KEY] sebagai endpoint.  Data yang diperlukan untuk register akan dikirimkan dengan bentuk json. Setelah registrasi berhasil, user akan dialihkan ke halaman profil. |

**2. Artikel**

| Fitur | Cerita |
| ------ | ------ |
| Lihat artikel | Fitur **melihat artikel** membutuhkan data artikel yang ada di *database*. Agar dapat mengambil data tersebut, digunakan metode **GET** untuk mengambil semua objek artikel yang dibuat, kemudian menampilkannya di aplikasi dalam bentuk daftar artikel. *Webservice* yang digunakan berasal dari url *https://pbp-f08.herokuapp.com/article/*. Selain itu, *models* yang digunakan adalah *Artikel*.|
| Tambah artikel | Fitur **tambah artikel** menggunakan metode **POST** untuk mengirimkan data ke webservice : *https://pbp-f08.herokuapp.com/article/add-article/*. Pada aplikasi, terdapat tampilan berupa kotak kosong yang dapat diisi dengan masukan pengguna. Fitur ini akan mengirimkan data berupa masukan dari admin RS untuk menambahkan artikel. Sama seperti fitur **melihat artikel**,  *models* yang digunakan adalah *Artikel*.|

**3.  Profil**

| Fitur | Cerita |
| ------ | ------ |
| Lihat profil | Fitur **Lihat Profil** dapat diakses ketika user telah melakukan login sehingga membutuhkan informasi yang terdapat di database dengan menggunakan metode **GET**. Models yang digunakan adalah *RumahSakit*. Halaman yang ditampulkan berisi deskripsi dari rumah sakit user, seperti nama, lokasi, ketersediaan vaksin dan bed, dan tombol **EDIT** yang akan redirect menuju halaman **Edit Profil**, yang dapat diakses pada webservice : *https://pbp-f08.herokuapp.com/profile*. |
| Edit profil | Fitur **Edit Profil** dapat diakses hanya ketika user telah melakukan login juga sama seperti fitur **lihat profil**. Fitur ini mengambil data di database dengan method **GET** dan disimpan kembali dengan method **POST**. Models yang digunakan adalah *RumahSakit*. Halaman yang ditampilkan adalah form untuk mengubah deskripsi dari rumah sakit user yang dapat diakses pada webservice : *https://pbp-f08.herokuapp.com/profile/edit-profile/* |

**4. Masukan**

| Fitur | Cerita |
| ------ | ------ |
| Tambah masukan | Fitur **Tambah Masukan** mengirimkan data masukan ke database dengan metode **POST**. Fitur masukan menggunakan _models_ Masukan *Webservice* yang digunakan ada di beberapa aplikasi yakni *https://pbp-f08.herokuapp.com/bed-rumahsakit*, https://pbp-f08.herokuapp.com, homepage,dan biodata rumah sakit.|

**5. Katalog Bed**

| Fitur | Cerita |
| ------ | ------ |
| Lihat Daftar RS |Fitur **lihat daftar RS** mengambil  data rumah sakit yang ada di *database* dengan metode **GET**. Lalu, menampilkan rumah sakit yang memiliki bed  di aplikasi dalam bentuk katalog. *Webservice* yang digunakan berasal dari url *https://pbp-f08.herokuapp.com/bed-rumahsakit*. Selain itu, *models* yang digunakan adalah *RumahSakit*. |
| Lihat Detail RS | Fitur **lihat detail RS** mengambil data objek RumahSakit berdasarkan _primary key_ rumah sakit di Katalog Bed dan menampilkan _fields_ dari objek rumah sakit. _Models_ yang digunakan adalah RumahSakit. _Webservice_ yang digunakan berasal dari url yang diberikan melalui fitur Katalog Bed.|

**6. Katalog Vaksin**

| Fitur | Cerita |
| ------ | ------ |
| Lihat Daftar RS | Fitur **lihat daftar RS** mengambil  data rumah sakit yang ada di *database* dengan metode **GET**. Lalu, menampilkan rumah sakit yang memiliki vaksin  di aplikasi dalam bentuk katalog. *Webservice* yang digunakan berasal dari url *https://pbp-f08.herokuapp.com/vaksin-rumahsakit*. Selain itu, *models* yang digunakan adalah *RumahSakit*. |
| Lihat Detail RS | Fitur **lihat detail RS** mengambil data objek RumahSakit berdasarkan _primary key_ rumah sakit di Katalog Vaksin dan menampilkan _fields_ dari objek rumah sakit. _Models_ yang digunakan adalah RumahSakit. _Webservice_ yang digunakan berasal dari url yang diberikan melalui fitur Katalog Vaksin.|


### Link APK Tahap I
https://drive.google.com/file/d/1Y16wBKQLHkuribU-WtcSfzQxDIUJhIbQ/view?usp=sharing

### Link APK Tahap II
https://drive.google.com/file/d/1A2expA9GKCvu185cpRU_Q_c03Yrk7J3H/view?usp=sharing
